import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import Heroes 1.0

Item {
    id: root

    scale: _mouseArea.pressed ? 0.95 : 1.0
    width: 200
    height: 100

    signal pressed()

    readonly property color delegateColor: isGoodCharacter ? "lightgreen" : "#E45252"
    readonly property bool isGoodCharacter: _main.state === "selectCharacter" &&
                                            index === 0 || type === "Wizard" || type === "Paladin"
    readonly property string type: hero ? hero.type : model.type
    readonly property string icon: hero ? hero.icon : model.icon
    property var hero

    Rectangle {
        anchors.centerIn: parent
        width: 180
        height: 90
        radius: 5
        color: delegateColor
    }
    Text {
        anchors.centerIn: parent

        font.pixelSize: 20
        text: type
    }

    MouseArea {
        id: _mouseArea

        anchors.fill: parent
        hoverEnabled: true

        onEntered: {
            if (_list.currentIndex !== index) {
                _list.currentIndex = index;
            }
        }
        onPressed: {
            root.pressed();
        }
    }
}
