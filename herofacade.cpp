#include <QDebug>
#include "herofacade.h"

HeroFacade::HeroFacade(QObject *parent) : QObject(parent) {}

AbstractHero *HeroFacade::createCaster(bool isGoodCharacter)
{
    return isGoodCharacter ? createGoodCaster() : createEvilCaster();
}

AbstractHero *HeroFacade::createWarrior(bool isGoodCharacter)
{
    return isGoodCharacter ? createGoodWarrior() : createEvilWarrior();
}

AbstractHero *HeroFacade::createHero(QString type)
{
    AbstractHero *hero;
    if (type == "Wizard" || type == "Necromancer") {
        hero = new CasterHeroProxy(resolveSource(type), type);
    } else {
        hero = new WarriorHeroProxy(resolveSource(type), type);
    }
    QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
    return hero;
}

bool HeroFacade::isGoodHero(AbstractHero *hero)
{
    return hero->type() == "Wizard" || hero->type() == "Paladin";
}

QUrl HeroFacade::resolveSource(QString type)
{
    QUrl icon;
    QStringList types = {"Wizard", "Paladin", "Necromancer", "Barbarian"};
    switch (types.indexOf(type)) {
    case 0:
        icon = "qrc:/assets/wizard.jpg";
        break;
    case 1:
        icon = "qrc:/assets/paladin.jpg";
        break;
    case 2:
        icon = "qrc:/assets/necromancer.jpg";
        break;
    case 3:
        icon = "qrc:/assets/barbarian.jpg";
        break;
    default:
        qWarning() << "Unhandled character : " << type;
    }
    return icon;
}

AbstractHero *HeroFacade::createEvilCaster() { AbstractHero *hero = new CasterHeroProxy("Saruman", "Evil army", resolveSource("Necromancer"), "Necromancer");
                                               QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                                                          return hero; }

AbstractHero *HeroFacade::createEvilWarrior() { AbstractHero *hero = new WarriorHeroProxy("Ork", "Baton attack", resolveSource("Barbarian"), "Barbarian");
                                                QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                                                            return hero; }

AbstractHero *HeroFacade::createGoodCaster() { AbstractHero *hero = new CasterHeroProxy("Gandalf", "Fireball", resolveSource("Wizard"), "Wizard");
                                               QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                                                          return hero; }

AbstractHero *HeroFacade::createGoodWarrior() { AbstractHero *hero = new WarriorHeroProxy("Aragorn", "Sword attack", resolveSource("Paladin"), "Paladin");
                                                QQmlEngine::setObjectOwnership(hero, QQmlEngine::ObjectOwnership::JavaScriptOwnership);
                                                                                            return hero; }

QObject *HeroFacade::instance(QQmlEngine *engine, QJSEngine *scriptEngine) {

    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    static HeroFacade *heroFacade = new HeroFacade();
    return heroFacade;
}
