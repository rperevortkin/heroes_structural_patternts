#ifndef WARRIORHERO_H
#define WARRIORHERO_H

#include <QUrl>
#include "abstracthero.h"

class WarriorHero : public AbstractHero
{

public:
    WarriorHero(QObject *parent = nullptr) : AbstractHero(parent) {}
    WarriorHero(QString name, QString hit, QUrl icon, QString type = "Paladin", QObject *parent = nullptr) :
        AbstractHero(parent) { m_name.append(name);
                               m_hit.append(hit);
                               m_icon = icon;
                               m_type = type; }

    QString name() const { return m_name; }
    QString hit() const { return m_hit; }
    QUrl icon() const { return m_icon; }
    QString type() const { return m_type; }

private:
    QString m_name;
    QString m_hit;
    QUrl m_icon;
    QString m_type;
};


class WarriorHeroProxy : public WarriorHero
{

public:
    WarriorHeroProxy(QObject *parent = nullptr) : WarriorHero(parent) {}
    WarriorHeroProxy(QUrl icon, QString type = "Paladin", QObject *parent = nullptr) : WarriorHero(parent) {
        m_icon = icon;
        m_type = type;
    }
    WarriorHeroProxy(QString name, QString hit, QUrl icon, QString type = "Paladin", QObject *parent = nullptr) :
        WarriorHero(parent) { m_warrior = new WarriorHero(name, hit, icon, type); }

    QString name() const { return m_warrior ? "Warrior " + m_warrior->name() : "Proxy Warrior name"; }
    QString hit() const { return m_warrior ? "Warrior " + m_warrior->hit() : "Proxy Warrior hit"; }
    QUrl icon() const { return m_warrior ? m_warrior->icon() : m_icon; }
    QString type() const { return m_warrior ? m_warrior->type() : m_type; }

private:
    QUrl m_icon;
    QString m_type;
    WarriorHero *m_warrior = nullptr;
};



#endif // WARRIORHERO_H
