#ifndef HEROFACADE_H
#define HEROFACADE_H

#include <QObject>
#include <QQmlEngine>
#include <QUrl>

#include "abstracthero.h"
#include "casterhero.h"
#include "warriorhero.h"

class HeroFacade : public QObject
{

    Q_OBJECT
public:
    HeroFacade(QObject *parent = nullptr);

    Q_INVOKABLE AbstractHero *createCaster(bool isGoodCharacter);
    Q_INVOKABLE AbstractHero *createWarrior(bool isGoodCharacter);
    Q_INVOKABLE AbstractHero *createHero(QString type);

    Q_INVOKABLE bool isGoodHero(AbstractHero *hero);
    Q_INVOKABLE QUrl resolveSource(QString type);

    AbstractHero *createEvilCaster();
    AbstractHero *createEvilWarrior();
    AbstractHero *createGoodCaster();
    AbstractHero *createGoodWarrior();

    static QObject *instance(QQmlEngine *engine, QJSEngine *scriptEngine);

};
#endif // HEROFACADE_H
