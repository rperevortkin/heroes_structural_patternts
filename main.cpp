#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "abstracthero.h"
#include "casterhero.h"
#include "warriorhero.h"
#include "herofacade.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterUncreatableType<AbstractHero>("Heroes", 1, 0, "AbstractHero", "can not instantiate AbstractHero in qml");
    qmlRegisterSingletonType<HeroFacade>("Heroes", 1, 0, "HeroFacade", &HeroFacade::instance);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
