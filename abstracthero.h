#ifndef ABSTRACTHERO_H
#define ABSTRACTHERO_H

#include <QObject>
#include <QUrl>

class AbstractHero : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString hit READ hit CONSTANT)
    Q_PROPERTY(QUrl icon READ icon CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
public:
    AbstractHero(QObject *parent = nullptr) : QObject(parent) {}

    virtual QString name() const = 0;
    virtual QString hit() const = 0;
    virtual QString type() const = 0;
    virtual QUrl icon() const = 0;
};

#endif // ABSTRACTHERO_H
