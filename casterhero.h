#ifndef CASTERHERO_H
#define CASTERHERO_H

#include <QUrl>
#include "abstracthero.h"

class CasterHero : public AbstractHero
{

public:
    CasterHero(QObject *parent = nullptr) : AbstractHero(parent) {}
    CasterHero(QString name, QString hit, QUrl icon, QString type = "Wizard", QObject *parent = nullptr) :
        AbstractHero(parent) { m_name = name;
                               m_hit = hit;
                               m_icon = icon;
                               m_type = type; }

    QString name() const { return m_name; }
    QString hit() const { return m_hit; }
    QUrl icon() const { return m_icon; }
    QString type() const { return m_type; }

private:
    QString m_name;
    QString m_hit;
    QUrl m_icon;
    QString m_type;
};

class CasterHeroProxy : public CasterHero
{

public:
    CasterHeroProxy(QObject *parent = nullptr) : CasterHero(parent) {}
    CasterHeroProxy(QUrl icon, QString type = "Wizard", QObject *parent = nullptr) : CasterHero(parent) {
        m_icon = icon;
        m_type = type;
    }
    CasterHeroProxy(QString name, QString hit, QUrl icon, QString type = "Wizard", QObject *parent = nullptr) :
        CasterHero(parent) { m_caster = new CasterHero(name, hit, icon, type); }

    QString name() const { return m_caster ? "Caster " + m_caster->name() : "Proxy Caster name"; }
    QString hit() const { return m_caster ? "Caster " + m_caster->hit() : "Proxy Caster hit"; }
    QUrl icon() const { return m_caster ? m_caster->icon() : m_icon; }
    QString type() const { return m_caster ? m_caster->type() : m_type; }

private:
    QUrl m_icon;
    QString m_type;
    CasterHero *m_caster = nullptr;
};

#endif // CASTERHERO_H
